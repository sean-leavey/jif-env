#!/usr/bin/env python3

from setuptools import setup

with open("README.md") as readme_file:
    readme = readme_file.read()

with open("HISTORY.md") as history_file:
    history = history_file.read()

__version__ = "0.5.3"

requirements = [
    "appdirs",
    "bottle",
    "pyepics"
]

setup(
    name="jif-env",
    version=__version__,
    description="EPICS adapter for JIF Lab environment sensors",
    long_description=readme + "\n\n" + history,
    author="Sean Leavey",
    author_email="sean.leavey@ligo.org",
    url="https://git.ligo.org/sean-leavey/jif-env",
    packages=[
        "server"
    ],
    package_dir={
        "server": "server"
    },
    package_data={
        "server": ['jif-env.conf.dist']
    },
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'jif-env-server = server.__main__:run'
        ]
    },
    license="GPLv3",
    zip_safe=False,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
    ]
)
