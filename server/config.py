"""Configuration parser and defaults"""

import os.path
import logging
import re
from configparser import RawConfigParser
import pkg_resources
import appdirs

LOGGER = logging.getLogger("config")

class Config(RawConfigParser):
    """Configuration class"""

    PROJECT_NAME = "jif-env"
    CONFIG_FILENAME = PROJECT_NAME + ".conf"
    DEFAULT_CONFIG_FILENAME = CONFIG_FILENAME + ".dist"

    def __init__(self, *args, **kwargs):
        """Instantiate a new BaseConfig"""

        super(Config, self).__init__(*args, **kwargs)

        self._load()

    def _load(self):
        """Load and parse a user config file"""

        # load default config first, then overwrite with user changes
        self._load_default_config()

        user_config = self.user_config_path

        # check the config file exists
        if not os.path.isfile(user_config):
            self._create_user_config(user_config)

        self._load_config(user_config)

    def _load_config(self, path):
        """Load and parse a config file

        :param path: config file path
        :type path: str
        """

        with open(path) as obj:
            LOGGER.debug("reading config from %s", path)
            self.read_file(obj)

    def _load_default_config(self):
        """Load and parse the default config file"""

        self._load_config(pkg_resources.resource_filename(__name__,
            self.DEFAULT_CONFIG_FILENAME))

    @property
    def user_config_path(self):
        """Find the path to the config file

        This creates the config file if it does not exist, using the distributed
        template.

        :return: path to user config file
        :rtype: str
        """

        config_dir = appdirs.user_config_dir(self.PROJECT_NAME)

        return os.path.join(config_dir, self.CONFIG_FILENAME)

    @classmethod
    def _create_user_config(cls, config_file):
        """Create empty config file in user directory

        :param config_file: path to config file
        :type config_file: str
        """

        directory = os.path.dirname(config_file)

        # create user config directory
        if not os.path.exists(directory):
            os.makedirs(directory)

        LOGGER.debug("creating empty config file at %s", directory)

        # touch file
        open(config_file, 'w').close()

    @property
    def mac_addresses(self):
        """Get MAC address keys"""

        return [key for key in self.keys() if self.is_mac(key)]

    @staticmethod
    def is_mac(mac):
        """Check if the specified string is a valid MAC address"""

        mac = str(mac).lower()

        return bool(re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac))
