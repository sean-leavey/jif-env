"""RESTful EPICS API for environment sensors"""

import sys
import os
import time
import logging
import json
import pkg_resources
from bottle import Bottle, request, abort

from .config import Config

# package version
__version__ = pkg_resources.require("jif-env")[0].version

# load config
CONFIG = Config()

# create logger
logger = logging.getLogger("server")

class EnvServer(object):
    def __init__(self):
        # create web app and routes
        self.app = Bottle()
        self.create_routes()

        self.start_time = None
        self.last_check_ins = {}

    def run(self):
        host = str(CONFIG["server"]["host"])
        port = int(CONFIG["server"]["port"])

        logger.info("Starting web server")
        self.start_time = time.time()
        self.app.run(host=host, port=port)

    def create_routes(self):
        self.app.route("/monitor/<sensor>", method="POST",
                       callback=self.handle_sensor_data)
        self.app.route("/info", method="GET", callback=self.info)

    def handle_sensor_data(self, sensor):
        sensor = sensor.lower()

        logger.info("Request \"%s\" from %s", sensor, request.remote_addr)

        if sensor == "dust":
            self.handle_dust_data(request)
        elif sensor == "env":
            self.handle_env_data(request)
        else:
            abort(404, "The specified sensor does not exist.")

    def handle_meta_data(self, req):
        mac = req.json["mac"].lower()

        if mac not in CONFIG.mac_addresses:
            abort(404, "The server is not configured to handle the specified "
                       "MAC address")

        # set check in
        self.last_check_ins[mac] = time.time()

        logger.info("Data specifies MAC=%s, version=%s",
            mac, req.json["version"])

    def handle_dust_data(self, req):
        self.handle_meta_data(req)

        logger.info("Dust data: "
            "dust1=%i, dust2=%i",
            req.json["dust1"], req.json["dust2"])

    def handle_env_data(self, req):
        self.handle_meta_data(req)

        logger.info("Environment data: "
            "temperature=%f, pressure=%f, humidity=%f, light=%i",
            req.json["temperature"], req.json["pressure"],
            req.json["humidity"], req.json["light"])

    def info(self):
        check_ins = ["%s: %f" % (mac, check_in)
                     for mac, check_in in self.last_check_ins.items()]
        labels = ["%s: %s" % (mac, str(CONFIG[mac]["label"]))
                     for mac in CONFIG.mac_addresses]

        data = {"server_start_time": self.start_time,
                "server_version": __version__,
                "sensors": ", ".join(labels),
                "last_check_ins": ", ".join(check_ins)}

        return json.dumps(data)
