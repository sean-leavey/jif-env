import sys
import logging
from logging.handlers import TimedRotatingFileHandler

from .config import Config
from .server import EnvServer

# load config
CONFIG = Config()

# get root logger
logger = logging.getLogger()

# open log file with handler
try:
    handler = TimedRotatingFileHandler(
        CONFIG['logging']['log_file'],
        when="D",
        backupCount=int(CONFIG["logging"]["file_count"]))
except PermissionError as e:
    logger.error("Log file at %s cannot be modified. Check it exists and that the "
    "current user has write permissions." % CONFIG['logging']['log_file'])
    sys.exit(1)

# set log formatter
formatter = logging.Formatter(CONFIG['logging']['log_format'])
handler.setFormatter(formatter)

# add log handler
logger.addHandler(handler)
logger.setLevel(logging.getLevelName(CONFIG['logging']['log_level'].upper()))

# find configured MAC addresses
if not len(CONFIG.mac_addresses):
    logger.error("No MAC addresses configured; exiting")
    sys.exit(1)

logger.info("Found configuration for the following MAC addresses: %s",
            ", ".join(CONFIG.mac_addresses))

def run():
    # start server
    rest_server = EnvServer()
    rest_server.run()
